import cv2
import numpy as np
from helper_functions import *

video = cv2.VideoCapture("MOT20-02-raw.webm")
_, original_image_BGR = video.read()
#original_image_RGB = cv2.cvtColor(original_image_BGR, cv2.COLOR_BGR2RGB)
#main_header = cv2.imread('templates/main_header.jpg')
#original_image_BGR = cv2.resize(original_image_BGR, (800, 500))
image_width = original_image_BGR.shape[1]
image_height = original_image_BGR.shape[0]

original_image_BGR_copy = original_image_BGR.copy()
#original_image_RGB_copy = original_image_RGB.copy()

print('image Shape', original_image_BGR.shape)

source_points = get_points(original_image_BGR_copy, 4, image_size=(image_width,image_height))
print(source_points)
src = np.float32(source_points)
print("Src: ", src)


for point in source_points:
    cv2.circle(original_image_BGR, tuple(point), 8, (255, 0, 0), -1)

points = source_points.reshape((-1,1,2)).astype(np.int32)
cv2.polylines(original_image_BGR, [points], True, (0,255,0), thickness=4)
dst=np.float32([(0.4,0.82), (0.80, 0.82), (0.80,0.87), (0.4,0.87)])

dst_size=(800,1080)
dst = dst * np.float32(dst_size)
print("DTS: ",dst)
H_matrix = cv2.getPerspectiveTransform(src, dst)
print("The perspective transform matrix:")
print(H_matrix)

warped = cv2.warpPerspective(original_image_BGR, H_matrix, dst_size)
img = cv2.resize(warped, (400,600))
cv2.imshow("OG", original_image_BGR)
cv2.imshow("Test", img)
cv2.waitKey(0)