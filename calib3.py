import cv2
import numpy as np
from helper_functions import *

video = cv2.VideoCapture("people.mp4")
_, original_image_BGR = video.read()
#original_image_BGR = cv2.resize(original_image_BGR, (1440, 540))
#original_image_RGB = cv2.cvtColor(original_image_BGR, cv2.COLOR_BGR2RGB)
#main_header = cv2.imread('templates/main_header.jpg')

image_width = original_image_BGR.shape[1]
image_height = original_image_BGR.shape[0]

original_image_BGR_copy = original_image_BGR.copy()
#original_image_RGB_copy = original_image_RGB.copy()

print('image Shape', original_image_BGR.shape)
'''
source_points = get_points(original_image_BGR_copy, 4, image_size=(image_width,image_height))
print(source_points)
src = np.float32(source_points)
print("Src: ", src)

src = np.float32([[ 401.,  132.],
 [1492.,  106.],
 [1883.,  727.],
 [  35.,  763.]])
 '''
#src = np.float32([[142., 298.], [784., 315.], [811., 371.],[ 82., 347.]])  #MOT clip
src = np.float32([[ 401.,  132.], [1492.,  106.], [1883.,  727.], [  35.,  763.]]) #people.mp4
#for point in source_points:
    #cv2.circle(original_image_BGR, tuple(point), 8, (255, 0, 0), -1)

points = src.reshape((-1,1,2)).astype(np.int32)
cv2.polylines(original_image_BGR, [points], True, (0,255,0), thickness=4)
#dst=np.float32([(0.25,0.47), (0.78, 0.47), (0.78,0.77), (0.25,0.77)])
#dst=np.float32([(0.2,0.82), (0.80, 0.82), (0.80,0.87), (0.2,0.87)]) # cho MOT clip
dst=np.float32([(0.25,0.47), (0.78, 0.47), (0.78,0.77), (0.25,0.77)]) #cho people.mp4
dst_size=(800,1080)
dst = dst * np.float32(dst_size)

print("DTS: ",dst)
H_matrix = cv2.getPerspectiveTransform(src, dst)
print("The perspective transform matrix:")
print(H_matrix)
class_names = []
with open("models/coco.names", "r") as f:
    class_names = [cname.strip() for cname in f.readlines()]
net = cv2.dnn.readNet("models/yolov3.weights", "models/yolov3.cfg")
net.setPreferableBackend(cv2.dnn.DNN_BACKEND_CUDA)
net.setPreferableTarget(cv2.dnn.DNN_TARGET_CUDA)
model = cv2.dnn_DetectionModel(net)
model.setInputParams(size=(416, 416), scale=1 / 255)
confidence_threshold = 0.5
nms_threshold = 0.5
min_distance = 25
image_height, image_width = original_image_BGR.shape[:2]
classes, _ , boxes = model.detect(original_image_BGR, confidence_threshold, nms_threshold)
list_boxes = []

for (classid,box) in zip(classes, boxes):
 if class_names[classid[0]] != 'person': break
 x, y, w, h = box
 center_x, center_y = int(x+w/2), int(y+h/2)
 list_boxes.append([x, y, w, h, center_x, center_y])

warped = cv2.warpPerspective(original_image_BGR, H_matrix, dst_size)
birds_eye_points = compute_point_perspective_transformation(H_matrix, list_boxes)
green_box, red_box, pp, ppp = get_red_green_boxes(min_distance, birds_eye_points, list_boxes)
birds_eye_view_image = get_birds_eye_view_image(green_box, red_box,eye_view_height=image_height,eye_view_width=image_width//2, points=pp, image=warped)
box_red_green_image = get_red_green_box_image(original_image_BGR.copy(),green_box, red_box, ppp)
combined_image = np.concatenate((birds_eye_view_image,box_red_green_image), axis=1)
resize_combined_image = cv2.resize(combined_image, (1440, 540))


#img = cv2.resize(warped, (400,600))
#resize = cv2.resize(original_image_BGR, (700,500))
#cv2.imshow("OG", resize)
cv2.imshow("Test", resize_combined_image)
cv2.waitKey(0)