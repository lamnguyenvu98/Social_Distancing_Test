import cv2
import numpy as np
from helper_functions import *

original_image_BGR = cv2.imread('MOT20_02_raw_frame_100.jpg')
original_image_RGB = cv2.cvtColor(original_image_BGR, cv2.COLOR_BGR2RGB)
#main_header = cv2.imread('templates/main_header.jpg')

image_width = original_image_RGB.shape[1]
image_height = original_image_RGB.shape[0]

original_image_BGR_copy = original_image_BGR.copy()
original_image_RGB_copy = original_image_RGB.copy()

#print('image Shape', original_image_RGB.shape)

# points for MOT20_02_raw.mp4 or MOT20_02_raw_frames
source_points = np.float32([[142., 298.],
                           [784., 315.],
                           [811., 371.],
                           [ 82., 347.]])

for point in source_points:
    cv2.circle(original_image_RGB_copy, tuple(point), 8, (255, 0, 0), -1)

points = source_points.reshape((-1,1,2)).astype(np.int32)
cv2.polylines(original_image_RGB_copy, [points], True, (0,255,0), thickness=4)

src=source_points
dst=np.float32([(0.5,0.82), (0.80, 0.82), (0.80,0.87), (0.5,0.87)])

dst_size=(800,1080)
dst = dst * np.float32(dst_size)
#dst=np.float32([(160.,885.6), (640, 885.6), (640,1000.6), (160,1000.6)])
print(dst)
H_matrix = cv2.getPerspectiveTransform(src, dst)
#print("The perspective transform matrix:")
#print(H_matrix)
print("Height", image_height)
print("Width", image_width)
warped = cv2.warpPerspective(original_image_RGB_copy, H_matrix, dst_size)
warped = cv2.resize(warped, (400,600))
cv2.imshow("Test", warped)
cv2.imshow("OG image", original_image_RGB_copy)
cv2.waitKey(0)